import 'package:flutter/material.dart';
class SwitchButton extends StatefulWidget {
  const SwitchButton({super.key});

  @override
  State<SwitchButton> createState() => _State();
}

class _State extends State<SwitchButton> {
  bool notification = true ;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Switch(
                activeColor: Colors.orangeAccent,
                value: notification,
                onChanged: (value){
                  setState(() {
                    notification=value;
                  });
                },
              )
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fchoolfood/api/productapi.dart';
import 'package:fchoolfood/cart/components/check_out_card.dart';
import 'package:fchoolfood/customNavBar.dart';
import 'package:fchoolfood/models/Cart.dart';
import 'package:fchoolfood/models/FoodProduct.dart';

import '../../../size_config.dart';
import 'cart_card.dart';
import 'package:provider/provider.dart';
class Body extends StatefulWidget {
  final Function() parentMethod;
  const Body({
    Key? key,
    required this.parentMethod
  }) : super(key: key);
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {

  @override
  Widget build(BuildContext context) {
    final cartProvider = Provider.of<CartProvider>(context);
    List<Cart> demoCarts=cartProvider.cart;
    return Scaffold(
      body: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: ListView.builder(
          itemCount: demoCarts.length ,
          itemBuilder: (context, index) => Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Dismissible(
              key: Key(demoCarts[index].product.id.toString()),
              direction: DismissDirection.endToStart,
              onDismissed: (direction) {
                setState(() {
                  // addCart();
                  demoCarts.removeAt(index);
                  cartProvider.setLocal();
                  print(demoCarts.length);
                });
              },
              background: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  color: Color(0xFFFFE6E6),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Row(
                  children: [
                    Spacer(),
                    SvgPicture.asset("assets/icons/Trash.svg"),
                  ],
                ),
              ),
              child: CartCard(cart: demoCarts[index],parentMethod: widget.parentMethod),
            ),
          ),
        ),
      ),
    );
  }
}

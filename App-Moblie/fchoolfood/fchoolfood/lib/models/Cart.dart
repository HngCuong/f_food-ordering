
import 'dart:ffi';
import 'package:fchoolfood/models/FoodProduct.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart';
import 'package:fchoolfood/api/post.dart';
import 'package:fchoolfood/api/postapi.dart';
import 'package:fchoolfood/api/productapi.dart';
import 'package:fchoolfood/models/FoodProduct.dart';
import "dart:convert";

class Cart {
  late Results product;
  late int numOfItem;

  Cart({required this.product, required this.numOfItem});

  Results get results => product;

  int get quantity => numOfItem;

  void  setQuantity() {
    numOfItem = numOfItem + 1;
  }
  void  setsubQuantity() {
    numOfItem = numOfItem - 1;
  }

  void increase() {
    numOfItem++;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['numOfItem'] = this.numOfItem;
    data['product'] = this.product;
    return data;
  }
  Cart.fromJson(Map<String, dynamic> json) {
    numOfItem = json['numOfItem'];
    Map<String, dynamic> jsonObj =  json['product'];
    product  = Results.fromJson(jsonObj);

  }
}




class CartProvider with ChangeNotifier {
  List<Cart> _results = [];
  List<Cart> get cart => _results;
  int get total => totalAmount();
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (_results != null) {
      data['Cart'] = _results!.map((v) => v.toJson()).toList();
    }
    return data;
  }
  fromJson(Map<String, dynamic> json) {
    _results.clear();
    if (json['Cart'] != null) {
      json['Cart'].forEach((v) {
        _results!.add(new Cart.fromJson(v));
      });
    }
  }
  void clearCart(){
    _results.clear();
    notifyListeners();
  }

  Future<void> clearItem() async
  {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('cart');
    notifyListeners();
  }
  Future<void> upLocal() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString('cart')!;
    Map<String, dynamic> jsonList = json.decode(stringValue);
    print(stringValue + "decode xong");
    fromJson(jsonList);

  }
  Future<void> setLocal() async
  {
    String json = jsonEncode(toJson());
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("cart", json);
    print(json);
    print("Thanh Cong");
  }

  void results(Cart value) {
    _results.add(value);
    setLocal();
    notifyListeners();
  }
   int totalAmount(){
    int a = 0;
    for(var item in _results ){
      a += (item.quantity * item.results.price!);
    }
    notifyListeners();
    return a;
  }
}

class ItemProvider with ChangeNotifier {
  List<Resultss> _results = [];
  List<Resultss> get item => _results;
  void addData() {
    print("chay add Item trong provider");
    fetchItem().then((value) => {
      for (var item in value) {_results.add(item)},

    });
    notifyListeners();
  }
  ItemProvider() {
    addData();
  }


}


import 'package:fchoolfood/api/postapi.dart';
import 'package:fchoolfood/api/productapi.dart';
import 'package:fchoolfood/models/Cart.dart';
import 'package:fchoolfood/order_detail.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/api/post.dart';
import 'package:fchoolfood/customNavBar.dart';

import 'provider/sign_in_provider.dart';

class HistoryOrder extends StatefulWidget {
  const HistoryOrder({super.key});

  @override
  State<HistoryOrder> createState() => _HistoryOrderState();
}

class _HistoryOrderState extends State<HistoryOrder> {

  @override
  Widget build(BuildContext context) {
    late bool _isSignedIn;
    _isSignedIn = context.read<SignInProvider>().isSignedIn;
    List<Resultss> postData = [];
    final resultProvider = Provider.of<ItemProvider>(context);
    fetchItem().then((value) => {
      print(value.length.toString()),
      for (var item in value) {
        print(item.orderName.toString()),
        postData.add(item)
      }
    });
    print(postData.length.toString());
    return FutureBuilder(
        future: Future.delayed(Duration(seconds: 1)), // Delay for 1 second
        builder: (BuildContext context, AsyncSnapshot snapshot) {
    if (snapshot.connectionState == ConnectionState.waiting) {
    return Container(
      height: 1000,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: CircularProgressIndicator(
          ),
        ),
      ),
    ); // Show a progress indicator while waiting
    } else {
      return Scaffold(
        body: SafeArea(
            child: _isSignedIn == true ? Stack(
              children: [
                Material(
                  child: Container(
                    padding: EdgeInsets.all(16),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          for (var postData in postData)
                            Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Card(
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    children: [
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Image.asset(
                                        "assets/images/virtual/vector1.png",
                                        fit: BoxFit.cover,
                                        height: 100,
                                      ),
                                      Expanded(
                                        child: Column(
                                          children: [
                                            Text(
                                              "ID: ${postData.orderName}",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            SizedBox(
                                              height: 35,
                                            ),
                                            Text(
                                              "Total: ${postData
                                                  .totalAmount}\$",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text(
                                            "${postData.checkInDate?.substring(
                                                0, 10)}",
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          ElevatedButton(
                                            onPressed: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        OrderDetail(
                                                          order: postData,
                                                        )),
                                              );
                                            },
                                            style: ElevatedButton.styleFrom(
                                              primary: Colors.orangeAccent,
                                            ),
                                            child: Container(
                                              width: 60,
                                              child: Padding(
                                                padding: EdgeInsets.all(10),
                                                child: Center(
                                                  child: Text("Detail Order"),
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          SizedBox(
                            height: 100,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                    bottom: 0,
                    left: 0,
                    child: CustomNavBar(
                      key: ValueKey('someValue'),
                      more: true,
                    )
                )
              ],
            ) :
            Stack(
              children: [
                Container(
                  height: 900,
                ),
                Positioned(
                    bottom: 0,
                    left: 0,
                    child: CustomNavBar(
                      key: ValueKey('someValue'),
                      more: true,
                    )
                )
              ],
            )
        ),
      );
    }
    }
    );
  }
}

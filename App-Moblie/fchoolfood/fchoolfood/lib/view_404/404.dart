import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../utils_404/my_colors.dart';
import '../view_404/first_screen.dart';

class page404 extends StatelessWidget {
  const page404({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Page 404 error UI',
      theme: ThemeData(
          textTheme: const TextTheme(
        headline1: TextStyle(
          fontSize: 20,
          color: MyColors.mainTextColor,
          fontWeight: FontWeight.w500,
        ),
        headline2: TextStyle(
          fontSize: 80,
          color: MyColors.mainTextColor,
          fontWeight: FontWeight.bold,
        ),
        headline3: TextStyle(
          fontSize: 27,
          color: MyColors.mainTextColor,
          fontWeight: FontWeight.bold,
        ),
        headline4: TextStyle(
          fontSize: 18,
          color: MyColors.mainSubTitileColor,
          fontWeight: FontWeight.w400,
        ),
        headline5: TextStyle(
          color: Colors.white,
          fontSize: 19,
        ),
      )),
      home: const FirstScreen(),
    );
    ;
  }
}

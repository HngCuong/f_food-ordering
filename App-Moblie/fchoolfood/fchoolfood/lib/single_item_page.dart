import 'dart:async';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/api/productapi.dart';
import 'package:fchoolfood/customNavBar.dart';
import 'package:fchoolfood/models/Cart.dart';
import 'package:fchoolfood/models/FoodProduct.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SingleItemPage extends StatefulWidget {
  final Results idItem;

  const SingleItemPage({super.key, required this.idItem});

  @override
  State<SingleItemPage> createState() => _SingleItemPageState();
}

class _SingleItemPageState extends State<SingleItemPage> {
  bool _showScaffold = false;
  Results? postData;

  String img =
      "https://firebasestorage.googleapis.com/v0/b/flutter-8c9ad.appspot.com/o/";
  String img1 = ".png?alt=media&token=";

  @override
  void initState() {
    super.initState();
    postData = widget.idItem;
  }

  @override
  Widget build(BuildContext context) {
    final cartProvider = Provider.of<CartProvider>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 10, left: 10, right: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Icon(
                              CupertinoIcons.cart_fill,
                              color: Colors.grey,
                              size: 32,
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 70),
                        child: Image.network(
                          "${postData?.image}",
                          height: 250,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        topLeft: Radius.circular(20),
                      )),
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "${postData?.name}",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 23,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Food is a fundamental part of our lives, providing sustenance, pleasure, and fuel for our bodies. With so many different cuisines and flavors to choose from, there's something for everyone to enjoy. Whether you're a fan of spicy dishes, comfort food classics, or healthy and nutritious meals, food has the power to bring people together and create unforgettable experiences.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Column(
                  children: [
                    Text(
                      "Total Price : ${postData?.price}\$",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    InkWell(
                      onTap: () {
                        Fluttertoast.showToast(
                            msg: "The Item has been add to Cart",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0);
                        bool flag = false;
                        Cart a = new Cart(product: postData!, numOfItem: 1);
                        for (var item in cartProvider.cart) {
                          if (a.results.id == item.results.id) {
                            item.setQuantity();
                            flag = true;
                          }
                        }
                        if (!flag) {
                          cartProvider.results(a);
                        }
                      },
                      child: Container(
                        width: 150,
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                        decoration: BoxDecoration(
                            color: Colors.redAccent.shade200,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                                bottomLeft: Radius.circular(20))),
                        child: Row(
                          children: [
                            Text(
                              "Add to Cart",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Icon(
                              CupertinoIcons.cart_fill,
                              color: Colors.white,
                              size: 20,
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          Positioned(
              bottom: 0,
              left: 0,
              child: CustomNavBar(
                key: ValueKey('someValue'),
                menu: true,
              ))
        ],
      ),
    );
  }
}
